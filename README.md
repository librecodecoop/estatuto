# Estatuto LibreCode

A última versão do PDF pode ser baixada [aqui](https://gitlab.com/librecodecoop/estatuto/-/jobs/artifacts/master/file/estatuto.pdf?job=build)

## Jobs

Os jobs são executados automaticamente a cada build de acordo com o arquivo [`.gitlab-ci.yml`](.gitlab-ci.yml)

## Gerar o PDF usando Docker

```
docker build . -t latex
docker run -it --rm -v $PWD:/latex latex sh -c "make"
```

## Gerar o PDF usando docker-compose

```
docker-compose up
```

## Gerar o PDF local

Instale as dependências que estão descritas no Makefile ou no Dockerfile e depois execute o comando [make](Makefile) na raiz do projeto.

## Edição local

Sugestão: Use a extensão LaTeX para o Codium